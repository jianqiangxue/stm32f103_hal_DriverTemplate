#ifndef __PCF8574_HEADER__
#define __PCF8574_HEADER__

#include "hal_iic.h"

IIC_STATUS PCF8574ReadByte(const IIC_PIN *I, uint8_t *ch);
IIC_STATUS PCF8574WriteByte(const IIC_PIN *I, uint8_t ch);

#endif //__PCF8574_HEADER__
