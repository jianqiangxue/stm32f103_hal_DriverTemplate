#include "ds1339.h"

/*************笔记****[DS1339数据手册.pdf]************
1、P17-特性：如果之前有发送命令地址，则读数据命令，会从上个地址顺延，并读取数据。
             直到发送了新的命令地址。
2、P17-器件读写操作流程
*******************************************************/

/*声明内部函数*/
uint8_t ConvertDataToSet(const T_DS1339TIME *time, uint8_t regType);
void ConvertDataToGet(uint8_t rData, T_DS1339TIME *time, uint8_t regType);


/*********************************************
函数名：DS1339Init
功  能：初始化器件的IIC引脚
形  参：PIN     --IIC信息结构体
**********************************************/
void DS1339Init(const IIC_PIN *PIN)
{
    IIC_Init(PIN);
}

/******************************
函数名：DS1339GetTime
功  能：设置器件时间
形  参：PIN     --IIC信息结构体
        time    --存放器件时间的信息结构体
返回值：
备  注：特性，器件寄存器地址，每次写入1byte，就会自动递增到下一个地址。
*******************************/
IIC_STATUS DS1339SetTime(const IIC_PIN *PIN, const T_DS1339TIME *time)
{
    IIC_STATUS status;
    uint8_t i, sendData;

    osDelay(100);
    IIC_Start(PIN);
    IIC_WriteByte(PIN, PIN->ADDR);
    status = IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, REGADDR_SECONDS);
    status |= IIC_Wait_ACK(PIN);
    for(i = 0; i < 7; i++)
    {
        sendData = ConvertDataToSet(time, REGADDR_SECONDS + i); //时间格式转换
        IIC_WriteByte(PIN, sendData);
        status |= IIC_Wait_ACK(PIN);
    }
    IIC_Stop(PIN);
    if(status)
        return(IIC_WRITE_ERR);
    else
        return(IIC_OK);
}

/******************************
函数名：DS1339GetTime
功  能：从器件读取时间
形  参：PIN     --IIC信息结构体  
        time    --存放器件时间的信息结构体
返回值：
备  注：
*******************************/
IIC_STATUS DS1339GetTime(const IIC_PIN *PIN, T_DS1339TIME *time)
{
    T_DS1339TIME rTime;
    uint8_t i, rData[7];
    IIC_STATUS status;

    IIC_Start(PIN);
    IIC_WriteByte(PIN, PIN->ADDR);
    status = IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, REGADDR_SECONDS);
    status |= IIC_Wait_ACK(PIN);

    IIC_Start(PIN);
    IIC_WriteByte(PIN, PIN->ADDR | 0x01);
    status |= IIC_Wait_ACK(PIN);
    for(i = 0; i < 7; i++) //从器件寄存器中按顺序读取 秒，分，时，天(0-7)，日，月，年
    {
        if(i != 0 )
            IIC_ACK(PIN);
        IIC_ReadByte(PIN, &rData[i]);
    }
    IIC_NACK(PIN);
    IIC_Stop(PIN);

    /*时间转换*/
    for(i = 0; i < 7; i++)
    {
        ConvertDataToGet(rData[i], &rTime, REGADDR_SECONDS + i);
    }
    if(status)
    {
        return (IIC_READ_ERR);
    }
    else
    {
        *time = rTime;
        return (IIC_OK);
    }
}

/******************************************************************
函数名：ConvertDataToSet
功  能：设置器件时间，因为器件寄存器对时间格式有要求，
        所以需要对时间进行转换，具体看手册。
形  参：
        time    --向器件设置时间的信息结构体
        regType --时间类型
返回值：
备  注：DS1339数据手册.pdf --10页--寄存器说明
*******************************************************************/
uint8_t ConvertDataToSet(const T_DS1339TIME *time, uint8_t regType)
{
    uint8_t sendData = 0;

    switch(regType)
    {
        case REGADDR_SECONDS:
            if(time->second > 10)
            {
                sendData = ((time->second / 10) << 4) & 0x7F;
                sendData |= time->second % 10;
            }
            else
            {
                sendData = time->second;
            }
            break;

        case REGADDR_MINUTES:
            if(time->minute > 10)
            {
                sendData = ((time->minute / 10) << 4) & 0x7F;
                sendData |= time->minute % 10;
            }
            else
            {
                sendData = time->minute;
            }
            break;

        case REGADDR_HOURS:
            if(time->hour > 10)
            {
                sendData = ((time->hour / 10) << 4) & 0x1F;
                sendData |= time->hour % 10;
            }
            else
            {
                sendData = time->hour;
            }
            break;

        case REGADDR_DAY:
            sendData = time->weekday & 0x0E;
            break;

        case REGADDR_DATA:
            if(time->day > 10)
            {
                sendData = ((time->day / 10) << 4) & 0x3F;
                sendData |= time->day % 10;
            }
            else
            {
                sendData = time->day;
            }
            break;

        case REGADDR_MONTH_CENTURY:
            if(time->month > 10)
            {
                sendData = ((time->month / 10) << 4) & 0x1F;
                sendData |= time->month % 10;
            }
            else
            {
                sendData = time->month;
            }
            break;
        case REGADDR_YEAR:
            if(time->year > 10)
            {
                sendData = ((time->year / 10) << 4) & 0xF0;
                sendData |= time->year % 10;
            }
            else
            {
                sendData = time->year;
            }
            break;
        default:
            ;
    }
    return sendData;
}

/******************************************************************
函数名：ConvertDataToGet
功  能：从器件获取时间的需要转换，因为器件寄存器对时间格式有要求，
        所以需要对时间进行转换，具体看手册。
形  参：
        time    --存放器件时间的信息结构体
        regType --时间类型
返回值：
备  注：DS1339数据手册.pdf --10页--寄存器说明
*******************************************************************/
void ConvertDataToGet(uint8_t rData, T_DS1339TIME *time, uint8_t regType)
{
    switch(regType)
    {
        case REGADDR_SECONDS:
            time->second = (rData >> 4 & 0x07) * 10 + (rData & 0x0F);
            break;

        case REGADDR_MINUTES:
            time->minute = (rData >> 4 & 0x07) * 10 + (rData & 0x0F);
            break;

        case REGADDR_HOURS:
            time->hour = (rData >> 4 & 0x01) * 10 + (rData & 0x0F);
            break;

        case REGADDR_DAY:
            time->weekday = rData & 0x07;
            break;

        case REGADDR_DATA:
            time->day = (rData >> 4 & 0x03) * 10 + (rData & 0x0F);
            break;

        case REGADDR_MONTH_CENTURY:
            time->month = (rData >> 4 & 0x01) * 10 + (rData & 0x0F);
            break;

        case REGADDR_YEAR:
            time->year = (rData >> 4 & 0x0F) * 10 + (rData & 0x0F);
            break;

        default:
            ;
    }
}
