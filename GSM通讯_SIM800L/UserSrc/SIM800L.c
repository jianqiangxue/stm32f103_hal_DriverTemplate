/*************笔记****************
1、本SIM800L模块采用huart3(串口3)，然后huart1(串口1)作为调试输出。
2、CudeMX配置huart3：
   ------------------------------------------
   Mode        --> Asynchronous(异步)
   Baud Rate   --> 9600 Bit/s
   Word Length --> 8 Bit
   Parity      --> None
   Stop Bits   --> 1
   ------------------------------------------
   NVIC        --> 串口中断使能
   ------------------------------------------
   DMA         --> Add 增加RX TX
               --> Data Width --> Byte
   ------------------------------------------
3、需要FreeRTOS系统支持，需要"uartext.c"、"uartext.h"
4、本代码末尾有FreeRTOS任务模板。StartGSMTask()
5、
***********************************/
#include "SIM800L.h"
#include <stdlib.h>
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "uartext.h"

extern UART_HandleTypeDef huart3;//串口3
extern uint8_t SIM800L_Get_text[32];
extern uint8_t Address;

/*********************************************************
函数名：SIM800L_Check_Cmd
功  能：发送命令后,检测接收到的应答
形  参：str--期待的应答结果
返回值：0--没有得到期待的应答结果 *?*--期待应答结果的位置(str的位置)
备  注：
**********************************************************/
uint8_t* SIM800L_Check_Cmd(uint8_t *str)
{
    char *strx = 0;
    strx = strstr((const char*)SIM800L_Get_text, (const char*)str); //寻找文本(被寻找，欲寻找)
    return (uint8_t*)strx;
}


/********************************************
函数名：SIM800L_Send_Cmd
功  能：向GSM发送命令
形  参：cmd:发送的命令字符串(不需要添加回车了)
        ack:期待的应答结果,如果为空,则表示不需要等待应答
        waittime:等待时间(单位:100ms)
返回值：0--发送成功(得到了期待的应答结果)
        1--发送失败
备  注：
*********************************************/
uint8_t SIM800L_Send_Cmd(uint8_t *cmd, uint8_t *ack, u16 WaitTime)
{
    uint8_t res = 0;
    uint8_t TxBuffer[32];
    uint8_t len;

    sprintf((char *)TxBuffer, "%s\r\n", cmd);
    UartPutStr(&huart3, TxBuffer, strlen((char *)TxBuffer));//发给串口3


    if(ack && WaitTime)     //需要等待应答
    {
        while(--WaitTime)    //等待倒计时
        {
            osDelay(100);
            len = UartGetStr(&huart3, SIM800L_Get_text); //从串口3读取一次数据
            if(len > 1) //接收期待的应答结果
            {
                if(SIM800L_Check_Cmd(ack))
                {
                    break;//得到有效数据
                }
            }
        }
        if(WaitTime == 0)
        {
            res = 1;
        }
    }
    return res;
}


/*********************************************************
函数名：SIM800L_Info_Show
功  能：GSM检测(SIM卡准备和是否注册成功)
形  参：无
返回值：2--正常  其他--错误代码
备  注：
**********************************************************/
uint8_t SIM800L_Info_Show(void)
{
    static uint8_t sim_flag = 0;
    uint8_t TxBuffer[40];

    switch(sim_flag)
    {
        case 0:
            if(SIM800L_Send_Cmd("AT+CPIN?", "OK", 20)) //查询SIM卡是否在位
            {
                sprintf((char *)TxBuffer, "%d,02,05,Not SIM Crad\r\n", Address);
                UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
            }
            else
            {
                sim_flag = 1;
                sprintf((char *)TxBuffer, "%d,02,05,SIM Crad Yes\r\n", Address);
                UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
            }
            break;
        case 1:
            if(SIM800L_Send_Cmd("AT+CREG?", "+CREG: 0,1", 20)) //查询SIM卡网络是否已注册
            {
                sprintf((char *)TxBuffer, "%d,02,05,Network Registering!\r\n", Address);
                UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
            }
            else
            {
                sim_flag = 2;
                sprintf((char *)TxBuffer, "%d,02,05,Network Register Success!\r\n", Address);
                UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
            }
            break;
    }
    return sim_flag;
}

/********************************************
函数名：SIM800L_CallNum
功  能：拨打指定号码
形  参：*num--手机号码("18977011111")
返回值：无
备  注：无
*********************************************/
void SIM800L_CallNum(uint8_t *Num)
{
    uint8_t TxBuffer[20];
    sprintf((char *)TxBuffer, "ATD%s;\r\n", Num);
    UartPutStr(&huart3, TxBuffer, strlen((char *)TxBuffer));//发给串口3
}

/********************************************
函数名：SIM800L_CmdShowOff
功  能：指令不回显
形  参：无
返回值：无
备  注：无
*********************************************/
void SIM800L_CmdShowOff(void)
{
    uint8_t TxBuffer[10];
    sprintf((char *)TxBuffer, "ATE0\r\n");
    UartPutStr(&huart3, TxBuffer, strlen((char *)TxBuffer));//发给串口3
}


/********************************************
函数名：SIM800L_SendEN_SMS
功  能：设置TEXT文本模式发送英文短信
形  参：*phone--接收短信的号码  *text--短信内容
返回值：无
备  注：SIM800L_SendEN_SMS(“10086”,“123”)
*********************************************/
void SIM800L_SendEN_SMS(uint8_t *phone, uint8_t *text)
{
    uint8_t TxBuffer[32];

    SIM800L_Send_Cmd("AT+CMGF=1", "OK", 10);       //设置文本模式
    SIM800L_Send_Cmd("AT+CSCS=\"GSM\"", "OK", 10); //设置TE字符集为GSM

    sprintf((char *)TxBuffer, "AT+CMGS=\"%s\"\r\n", phone);
    SIM800L_Send_Cmd(TxBuffer, ">", 10);                     //发送短信命令+电话号码

    UartPutStr(&huart3, text, strlen((char *)text)); //发给串口3,发送短信内容到GSM模块

    osDelay(1000);                                   //必须增加延时,否则接收方接收信息不全
    if(SIM800L_Send_Cmd("\x1a\r\n", "+CMGS:", 100) == 0) //发送结束符,等待发送完成(最长等待10秒钟,因为短信长了的话,等待时间会长一些)
    {
        sprintf((char *)TxBuffer, "%d,02,05,SMS Send Success!\r\n", Address);
        UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
    }
    else
    {
        sprintf((char *)TxBuffer, "%d,02,05,SMS Send fail!\r\n", Address);
        UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
    }
}


///*********************************************
//函数名：StartGSMTask
//功  能：处理GSM相关功能
//形  参：
//返回值：
//备  注：【GSM】的返回信息到达
//类型码：05
//**********************************************/
//uint8_t SIM800L_Get_text[32];
//void StartGSMTask(void const * argument)
//{
//    uint8_t RxBuffer[32], TxBuffer[32];
//    uint16_t len;
//    osDelay(400);
//    /*GSM初始化部分*/
//    sprintf((char *)TxBuffer, "AT\r\n");
//    UartPutStr(&huart3, TxBuffer, strlen((char *)TxBuffer));//初始化GSM模块的波特率，发送任意字符使其自适应
////    while(SIM800L_Send_Cmd("AT", "OK", 100)) //检测是否应答AT指令
////    {
////        sprintf((char *)TxBuffer, "未检测到模块!!!\r\n");
////        UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
////        osDelay(800);
////        sprintf((char *)TxBuffer, "尝试连接模块...\r\n");
////        UartPutStr(&huart1, TxBuffer, strlen((char *)TxBuffer));//发给串口1方便调试
////        osDelay(400);
////    }
//    for(;;)
//    {
//        len = UartGetStr(&huart3, RxBuffer); //从串口3读取一次数据
//        if(len > 1)
//        {
//            memcpy(SIM800L_Get_text, RxBuffer, strlen((char *)RxBuffer));
//            UartPutStr(&huart1, RxBuffer, len);
//        }

//        osMessagePut(QueWdtHandle, 0x08, 10);
//        osDelay(2);
//    }
//}
