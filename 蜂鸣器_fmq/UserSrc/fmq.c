/*************笔记****************
1、CubeMX 定义任意一个引脚，作为数据脚，并对引脚作出如下配置：
   GPlO output level       －－High
   GPIO mode               －－Output Open Drain
   GPIO Pull-up/Pull-down  －－No pull-up and no pull-down
   Maximum output speed    －－LOW
   User label              －－fmq

2、三极管8550是一种常用的普通三极管。 它是一种低电压，大电流，小信号的PNP型硅三极管。
   因为该蜂鸣器用的是三极管8550，所以置低电平鸣叫。
   NPN高电平--导通，PNP低电平--导通；

3、应用函数：beep_play();//蜂鸣器响一声
             beep_ring(uint16_t cut)//蜂鸣器响N ms
***********************************/
#include "fmq.h"
#include "cmsis_os.h"
#include "main.h"

#define fmq_Delay(N) osDelay(N)
/******************
函数名：beep_play
功能：蜂鸣器响一声
*******************/
void beep_play(void)
{
    HAL_GPIO_WritePin(fmq_GPIO_Port, fmq_Pin, GPIO_PIN_RESET); //输出低
    fmq_Delay(100);
    HAL_GPIO_WritePin(fmq_GPIO_Port, fmq_Pin, GPIO_PIN_SET); //输出高
}
/******************
函数名：beep_play
功能：蜂鸣器长响一声
*******************/
void beep_ring(uint16_t cut)
{
    HAL_GPIO_WritePin(fmq_GPIO_Port, fmq_Pin, GPIO_PIN_RESET); //输出低
    fmq_Delay(cut);
    HAL_GPIO_WritePin(fmq_GPIO_Port, fmq_Pin, GPIO_PIN_SET); //输出高
}
