#ifndef _FMQ_H
#define _FMQ_H
#include "stm32f1xx_hal.h"

//void beep_init(void);
void beep_play(void);
void beep_ring(uint16_t cut);
#endif
