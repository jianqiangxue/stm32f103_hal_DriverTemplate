/*************笔记****************
1、CubeMX 定义任意四个引脚，作为ABCD相，并对引脚作出如下配置：
   GPlO output level       －－High
   GPIO mode               －－Output Push Pull
   GPIO Pull-up/Pull-down  －－No pull-up and no pull-down
   Maximum output speed    －－LOW
   User label              －－dianji_A/dianji_B/dianji_C/dianji_D
   ---------------------------------------------------------

2、本设计采用TTL输出（即高低电平）  低电平--停止  高电平--运行
   PB12---电机A相 信号脚
   PB13---电机B相 信号脚
   PB14---电机C相 信号脚
   PB15---电机D相 信号脚
3、
  正转 电机导通相序 D-C-B-A
  反转 电机导通相序 A-B-C-D
***********************************/
#include "Motor.h"
#include "cmsis_os.h"
#include "stm32f1xx_hal.h"

#define DJ_D(N) HAL_GPIO_WritePin(dianji_D_GPIO_Port,dianji_D_Pin,N==1?GPIO_PIN_SET:GPIO_PIN_RESET)
#define DJ_C(N) HAL_GPIO_WritePin(dianji_C_GPIO_Port,dianji_C_Pin,N==1?GPIO_PIN_SET:GPIO_PIN_RESET)
#define DJ_B(N) HAL_GPIO_WritePin(dianji_B_GPIO_Port,dianji_B_Pin,N==1?GPIO_PIN_SET:GPIO_PIN_RESET)
#define DJ_A(N) HAL_GPIO_WritePin(dianji_A_GPIO_Port,dianji_A_Pin,N==1?GPIO_PIN_SET:GPIO_PIN_RESET)
/******************
函数名：MotorCW
功能：顺时针转动
*******************/
void MotorCW(void)
{
    DJ_D(1);          //D相运行
    DJ_C(0);
    DJ_B(0);
    DJ_A(0);
    osDelay(4);      //转速调节

    DJ_D(0);
    DJ_C(1);          //C相运行
    DJ_B(0);
    DJ_A(0);
    osDelay(4);      //转速调节

    DJ_D(0);
    DJ_C(0);
    DJ_B(1);          //B相运行
    DJ_A(0);
    osDelay(4);      //转速调节

    DJ_D(0);
    DJ_C(0);
    DJ_B(0);
    DJ_A(1);          //A相运行
    osDelay(4);      //转速调节
}

/******************
函数名：MotorCCW
功能：逆时针转动
*******************/
void MotorCCW(void)
{
    DJ_A(1);
    DJ_B(0);
    DJ_C(0);
    DJ_D(0);
    osDelay(4);//转速调节
    DJ_A(0);
    DJ_B(1);
    DJ_C(0);
    DJ_D(0);
    osDelay(4);//转速调节
    DJ_A(0);
    DJ_B(0);
    DJ_C(1);
    DJ_D(0);
    osDelay(4);//转速调节
    DJ_A(0);
    DJ_B(0);
    DJ_C(0);
    DJ_D(1);
    osDelay(4);//转速调节
}
//停止转动
void MotorStop(void)
{
    DJ_D(0);
    DJ_C(0);
    DJ_B(0);
    DJ_A(0);
}

/******************
函数名：Open_Door
功能：利用马达模拟开门动作
形参：
返回值：
备注：
*******************/
void Open_Door(void)
{
    u8 i = 0;
    for(i=0;i<100;i++)
    {
        MotorCW();
        osDelay(10);
    }
}
