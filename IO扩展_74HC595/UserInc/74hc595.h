/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __74HC595_HEADER__
#define __74HC595_HEADER__


/* USER CODE BEGIN Includes */
#include "main.h"

/* USER CODE END Includes */

typedef struct
{
    GPIO_TypeDef* sck_port;
    uint16_t      sck_pin;
    GPIO_TypeDef* data_port;
    uint16_t      data_pin;
    GPIO_TypeDef* rck_port;
    uint16_t      rck_pin;
    GPIO_TypeDef* en_port;
    uint16_t      en_pin;
} T_HC595_PIN;

extern const T_HC595_PIN hc595;

/* Exported functions prototypes ---------------------------------------------*/
void hc595Init(const T_HC595_PIN *pin);
void hc595DeInit(const T_HC595_PIN *pin);
void hc595WriteStr(const T_HC595_PIN *pin, uint8_t  *data, uint16_t length);
#endif
