#ifndef _AT24CXX_HEAD_
#define _AT24CXX_HEAD_
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "hal_iic.h"
#include "cmsis_os.h" //FreeRTOS系统中调用osDelay()

#define PAGE_LENGTH   64       //C01,C02为8字节，C04及以上为16字节,C64 32
#define ADDR_LENGTH   2        //地址长度C01,C02为1个字节，C04及以上为2字节
#define AT24Cxx_ADDR  0xA0     //定义器件地址

#define AT24Cxx_WRITE_ENABLE    0x00  //使能数据写入
#define AT24Cxx_WRITE_DISABLE   0x01  //禁止数所写入


void AT24CxxInit(const IIC_PIN *PIN);
IIC_STATUS AT24CxxWriteByte(const IIC_PIN *PIN, uint16_t addr, uint8_t ch);
IIC_STATUS AT24CxxReadByte(const IIC_PIN *PIN, uint16_t addr, uint8_t *data);
IIC_STATUS AT24CxxWriteStr(const IIC_PIN *PIN, uint16_t addr, uint8_t *str, uint16_t len);
IIC_STATUS AT24CxxReadStr(const IIC_PIN *PIN, uint16_t addr, uint8_t *str, uint16_t len);

#endif
