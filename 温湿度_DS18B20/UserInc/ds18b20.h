#ifndef __DS18B20_H
#define __DS18B20_H
#include "stm32f1xx_hal.h"

#define  SkipROM                0xCC     //跳过ROM
#define  SearchROM              0xF0  //搜索ROM
#define  ReadROM                0x33  //读ROM
#define  MatchROM               0x55  //匹配ROM
#define  AlarmROM               0xEC  //告警ROM

#define  StartConvert           0x44  //开始温度转换，在温度转换期间总线上输出0，转换结束后输出1
#define  ReadScratchpad         0xBE  //读暂存器的9个字节
#define  WriteScratchpad        0x4E  //写暂存器的温度告警TH和TL
#define  CopyScratchpad         0x48  //将暂存器的温度告警复制到EEPROM，在复制期间总线上输出0，复制完后输出1
#define  RecallEEPROM           0xB8    //将EEPROM的温度告警复制到暂存器中，复制期间输出0，复制完成后输出1
#define  ReadPower              0xB4    //读电源的供电方式：0为寄生电源供电；1为外部电源供电

#define DS_PRECISION            0x7F   //精度配置寄存器 1f=9位; 3f=10位; 5f=11位; 7f=12位;
#define DS_AlarmTH              0x64
#define DS_AlarmTL              0x8A

#define DisableInt()        __set_PRIMASK(1)  //屏蔽除NMI和fault以外的所有中断
#define EnableInt()         __set_PRIMASK(0)  //打开所有中断

typedef struct
{
    GPIO_TypeDef       *PORT;
    uint16_t            PIN;
    int8_t              LastDiscrepancy;        //最终分支处
    uint8_t             LastDeviceFlag;
    uint64_t            LastRomId;
} T_OW_TYPE;

void    OWInitIO(T_OW_TYPE *DS, GPIO_TypeDef * port, uint16_t pin);
//多个器件在同一总线上时的访问函数
uint8_t OWFirst(T_OW_TYPE *DS, uint8_t *ROM_ID);
uint8_t OWNext(T_OW_TYPE *DS, uint8_t *ROM_ID);
void OWStartConvert(T_OW_TYPE *DS, uint8_t *ROM_ID);
int16_t OWReadTemperture(T_OW_TYPE *DS, uint8_t *ROM_ID);
//单个器件在总线上时的访问函数
void OWSingleInit(T_OW_TYPE *DS, GPIO_TypeDef * port, uint16_t pin);
int16_t OWSingleReadTemp(T_OW_TYPE *DS);

#endif
