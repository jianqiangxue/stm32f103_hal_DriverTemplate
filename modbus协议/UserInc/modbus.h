#ifndef _MODBUS_CRC16_HEAD_
#define _MODBUS_CRC16_HEAD_
#include "stdint.h"
#include "main.h"

#define   MODBUS_RESPONE_TIMEOUT    150


uint16_t ModbusCRC16(uint8_t *puchMsg,uint16_t usDataLen);

#endif
