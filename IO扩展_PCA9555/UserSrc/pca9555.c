#include "pca9555.h"
/******************
笔记：pca9555.pdf
1、P17-控制寄存器和命令字节
2、P17-特性：如果之前有发送命令地址，则读数据命令，会从上个地址顺延，并读取数据。直到发送了新的命令地址。
3、P10-通讯过程：IIC启动-->发送器件识别地址1byte(0bit=R1/W0)-->ACK-->发送器件寄存器地址-->ACK-->[读数据/写数据]1byte
*******************/
U_INPUT   EX_IN;
U_OUTPUT  EX_OUT;

/******************************
函数名：PCA9555Init
功  能：初始化器件两组IO口的引脚功能
形  参：PIN--器件信息结构体
返回值：
备  注：
*******************************/
IIC_STATUS PCA9555Init(const IIC_PIN *PIN)
{
    IIC_STATUS status;

    IIC_Init(PIN);
    IIC_Start(PIN);
    IIC_WriteByte(PIN, PIN->ADDR);             //发送器件识别地址+写
    status = IIC_Wait_ACK(PIN);
//  IIC_WriteByte(PIN, PCA9555_REG_POL0);
//  status |= IIC_Wait_ACK(PIN);
//  IIC_WriteByte(PIN, PCA9555_POLARITY0);
//  status |= IIC_Wait_ACK(PIN);
//  IIC_WriteByte(PIN, PCA9555_POLARITY1);
//  status |= IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, PCA9555_REG_CFG0);     //寄存器地址写入(配置PIN脚为输入或输出)
    status |= IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, PCA9555_DERECTION0);   //设置IO0的0-7引脚全部为输入模式，因为与拨码器连接，作为IPADDR使用。
    status |= IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, PCA9555_DERECTION1);   //设置IO1的0-3引脚为输入，4-7为引脚为输出。
    status |= IIC_Wait_ACK(PIN);
    IIC_Stop(PIN);

    if(status)
        return(IIC_WRITE_ERR);
    else
        return(IIC_OK);
}

/******************************
函数名：PCA9555GetPin
功  能：从器件读取引脚状态
形  参：PIN--器件信息结构体 pin--存放引脚状态结构体
返回值：
备  注：
*******************************/
IIC_STATUS PCA9555GetPin(const IIC_PIN *PIN, U_INPUT *pin)
{
    uint8_t ch[2];
    IIC_STATUS status;

    IIC_Start(PIN);
    IIC_WriteByte(PIN, PIN->ADDR);              //发送器件地址+写命令
    status = IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, PCA9555_REG_IN0);        //发送器件寄存器地址--0x00--输入寄存器0地址
    status |= IIC_Wait_ACK(PIN);

    IIC_Start(PIN);
    IIC_WriteByte(PIN, PIN->ADDR | 0x01);       //发送器件地址+读命令
    status |= IIC_Wait_ACK(PIN);
    IIC_ReadByte(PIN, &ch[0]);                  //读取[输入寄存器0]的数据
    IIC_ACK(PIN);
    IIC_ReadByte(PIN, &ch[1]);                  //读取[输入寄存器1]的数据(特性：地址自动顺延)
    IIC_NACK(PIN);
    IIC_Stop(PIN);

    if(status)
        return (IIC_READ_ERR);
    else
    {
        pin->input[0] = ch[0];
        pin->input[1] = ch[1];
        return (IIC_OK);
    }
}

/******************************
函数名：PCA9555SetPin
功  能：设置器件的扩展IO口电平状态
形  参：PIN--器件信息结构体   pout--存放引脚状态结构体
返回值：
备  注：设置的值，只对之前配置为输出模式的引脚有效，输入模式的引脚无效。
*******************************/
IIC_STATUS PCA9555SetPin(const IIC_PIN *PIN, const U_OUTPUT *pout)
{
    IIC_STATUS status;

    IIC_Start(PIN);
    IIC_WriteByte(PIN, PIN->ADDR);
    status = IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, PCA9555_REG_OUT0);
    status |= IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, pout->output[0]);
    status |= IIC_Wait_ACK(PIN);
    IIC_WriteByte(PIN, pout->output[1]);
    status |= IIC_Wait_ACK(PIN);
    IIC_Stop(PIN);
    osDelay(10);
    if(status)
        return (IIC_WRITE_ERR);
    else
        return (IIC_OK);
}
